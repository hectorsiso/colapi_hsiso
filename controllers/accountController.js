require('../config/config');
const requestJson = require('request-json');

exports.getAccounts = (req, res) => {
  var user_id = req.params.id;
  var httpCliente = requestJson.createClient(process.env.MLAB_URL);
  var queryString = `q={"user_id":"${user_id}"}`;
  var urlAccounts = `account?${queryString}&apiKey=${process.env.MLAB_APIKEY}`;
  httpCliente.get(urlAccounts, (err, resp, body) => {
    res.send(body);
  });
}

exports.getAccountById = (req, res) => {
  var id = req.params.id;
  var idaccount = req.params.idaccount;
  var httpCliente = requestJson.createClient(process.env.MLAB_URL);
  var urlAccounts = `account/${idaccount}?apiKey=${process.env.MLAB_APIKEY}`;
  httpCliente.get(urlAccounts, (err, resp, body) => {
    res.send(body);
  });
}
