require('../config/config');
const requestJson = require('request-json');

exports.getUsers = (req, res) => {
  var httpCliente = requestJson.createClient(process.env.MLAB_URL);
  var urlUsers = 'user?apiKey=' + process.env.MLAB_APIKEY;
  httpCliente.get(urlUsers, (err, resp, body) => {
    res.send(body);
  });
}

exports.getUserById = (req, res) => {
  var id = req.params.id;
  var httpCliente = requestJson.createClient(process.env.MLAB_URL);
  var urlUsers = `user/${id}?apiKey=${process.env.MLAB_APIKEY}`;
  httpCliente.get(urlUsers, (err, resp, body) => {
    res.send(body);
  });
}

exports.updateUser = (req, res) => {
  var id = req.params.id;
  var httpCliente = requestJson.createClient(process.env.MLAB_URL);
  var queryString = `q={"_id":{"$oid":"${id}"}}`;
  var urlUsers = `user?${queryString}&apiKey=${process.env.MLAB_APIKEY}`;
  var data = {
             "$set": req.body
           }
  httpCliente.put(urlUsers, data, (err, resp, body) => {
    res.send(body);
  });
}


exports.createUser = (req, res) => {
  var httpCliente = requestJson.createClient(process.env.MLAB_URL);
  var urlUsers = `user?apiKey=${process.env.MLAB_APIKEY}`;
  var data = req.body;
  httpCliente.put(urlUsers, data, (err, resp, body) => {
    res.send(body);
  });
}

exports.relatedsUsers = (req, res) => {
  var id = req.params.id;
  var data = req.body;
  var httpCliente = requestJson.createClient(process.env.MLAB_URL);
  var urlUsers = `user/${id}?apiKey=${process.env.MLAB_APIKEY}`;
  httpCliente.get(urlUsers)
  .then((result) => {
     var queryString = `q={ "email": { $in: ${JSON.stringify(result.body.relateds)} } }`;
     var urlRelatedUsers = `user?${queryString}&apiKey=${process.env.MLAB_APIKEY}`;
     console.log(JSON.stringify(result.body.relateds));
     httpCliente.get(urlRelatedUsers)
     .then((result) => {
        res.send(result.body);
     }).catch((err) => {
        res.send(404);
     });
  }).catch((err) => {

     res.send(404);
  });
}
