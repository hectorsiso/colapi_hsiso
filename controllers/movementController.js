require('../config/config');
const requestJson = require('request-json');

exports.getMovements = (req, res) => {
  var idaccount = req.params.idaccount
  var httpCliente = requestJson.createClient(process.env.MLAB_URL);
  var queryString = `q={"idcuenta":"${idaccount}"}`;
  var urlMovements = `movement?${queryString}&apiKey=${process.env.MLAB_APIKEY}`;
  console.log(urlMovements);
  httpCliente.get(urlMovements, (err, resp, body) => {
    res.send(body);
  });
}

exports.getMovementById = (req, res) => {
  var idmov = req.params.idmov;
  var httpCliente = requestJson.createClient(process.env.MLAB_URL);
  var urlMovements = `movement/${idmov}?apiKey=${process.env.MLAB_APIKEY}`;
  httpCliente.get(urlMovements, (err, resp, body) => {
    res.send(body);
  });
}
