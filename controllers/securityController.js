require('../config/config');
const requestJson = require('request-json');

exports.checkemail = (req, res) => {

  var email = req.body.email;
  var httpCliente = requestJson.createClient(process.env.MLAB_URL);
  var queryString = `q={"email":"${email}"}`;
  var urlUsers = `user?${queryString}&apiKey=${process.env.MLAB_APIKEY}`;
  console.log(queryString);
  httpCliente.get(urlUsers, (err, resp, body) => {
    if(body.length > 0){
      res.send({
        "_id": {"$oid":body[0]._id.$oid},
        "first_name":body[0].first_name,
        "last_name":body[0].last_name,
        "email":body[0].email
      });
    }else{
      res.send(404);
    }

  });

}


exports.login = (req, res) => {
  var email = req.body.email;
  var password = req.body.password;
  var httpCliente = requestJson.createClient(process.env.MLAB_URL);
  var queryString = `q={"email":"${email}","password":"${password}"}`;
  var urlUsers = `user?${queryString}&apiKey=${process.env.MLAB_APIKEY}`;
  console.log(urlUsers);
  var data = {
	         "$set": {
		              "logged": "true"
	                  }
              };
  httpCliente.get(urlUsers, data, (err, resp, body) => {
    console.log(body);
    res.send(body[0]);
    /*if(body.n == "1"){
      res.send({"logged":"true"});
    }else{
      //console.log("mando 403");
      res.send(503,{"logged":"false"});
    }*/
  });
}


exports.logout = (req, res) => {
  var email = req.body.email;
  var password = req.body.password;
  var httpCliente = requestJson.createClient(process.env.MLAB_URL);
  var queryString = `q={"email":"${email}","password":"${password}"}`;
  var urlUsers = `user?${queryString}&apiKey=${process.env.MLAB_APIKEY}`;
  console.log(urlUsers);
  var data = {
	         "$set": {
		              "logged": "false"
	                  }
              };
  httpCliente.put(urlUsers, data, (err, resp, body) => {
    console.log(body);
    if(body.n == "1"){
      res.send({"logged":"false"});
    }else{
      res.send(503);
    }
  });
}
