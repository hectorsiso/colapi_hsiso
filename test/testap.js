var mocha = require('mocha');
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server'); //para ejecutar el server del api

var should = chai.should();

chai.use(chaiHttp); //configurar chai con módulo http

describe('Pruebas Colombia', () => {
  it('BBVA funciona ', (done) => {
    chai.request('http://www.bbva.com')
        .get('/')
        .end((err, res) => {
            //console.log(res);
            //res.should.have.status(200);
            res.header['server'] == 'AmazonS3';
            done();
        })
  });
  it('FIFA funciona ', (done) => {
    chai.request('http://www.fifa.com')
        .get('/')
        .end((err, res) => {
            //console.log(res);
            //res.should.have.status(200);
            res.header['server'] == 'AmazonS3';
            done();
        })
  });
  it('Mi API funciona', (done) => {
    chai.request('http://127.0.0.1:3000/dev/v3')
        .get('/')
        .end((err, res) => {
            //console.log(res);
            res.should.have.status(404);
            done();
        })
  });
  it('API Array usuarios', (done) => {
    chai.request('http://127.0.0.1:3000/dev/v3')
        .get('/users')
        .end((err, res) => {
            //console.log(res);
            res.body.should.be.a('array');
            done();
        })
  });
  it('API al menos 1 usuario', (done) => {
    chai.request('http://127.0.0.1:3000/dev/v3')
        .get('/users')
        .end((err, res) => {
            //console.log(res);
            res.body.length.should.be.gte(1);
            done();
        })
  });
  it('API validar primer elemento', (done) => {
    chai.request('http://127.0.0.1:3000/dev/v3')
        .get('/users')
        .end((err, res) => {
            //console.log(res);
            res.body[0].should.have.property('first_name');
            res.body[0].should.have.property('last_name');
            done();
        })
  });
  it('API checkemail', (done) => {
    chai.request('http://127.0.0.1:3000/dev/v3')
        .post('/checkemail')
        .send({"email":"hjsiso@gmail.com"})
        .end((err, res, body) => {
            console.log(res.body);
            res.body.should.have.property('first_name');
            //res.body[0].should.have.property('last_name');
            done();
        })
  });
});
