# Imagen inicial a partir de la cual creamos nuestra imagen
FROM node

#Directoior del contenedor
WORKDIR /colapi_hsiso

# Añadimos contenido del proyecto en directorio contenedor
ADD . /colapi_hsiso

# Puerto escucha el contenedor
EXPOSE 3000

# Comandos para lanzar nuestra API REST 'colapi'
CMD ["node", "server.js"]
