const express = require('express');
const router = express.Router();
const usersFile = require('./users.json');

router.get('/:dev', (req, res) => {
  console.log(req.params);
  res.json({ response: usersFile[req.params.dev - 1]  });
});
router.get('/', (req, res) => {
  res.json({ response: req.query.dato});
});
router.post('/', (req, res) => {
  console.log(req.body);
  res.json({
    response: 'a POST request for CREATING questions',
    body: req.body
  });
});
router.put('/:id', (req, res) => {
    var idBuscar = req.params.id;
    var updateUser = req.body;
    for(i = 0; i < usersFile.length; i++) {
      console.log(usersFile[i].id);
      if(usersFile[i].id == idBuscar) {
        updateUser = usersFile[i];
        res.send({"msg" : "Usuario actualizado correctamente.", updateUser});
      }
    }
    res.send({"msg" : "Usuario no encontrado.", updateUser});
});
router.delete('/:id', (req, res) => {
    var idBuscar = req.params.id;
    var d = usersFile.splice(idBuscar-1, 1);
    d.length > 0 ? res.send(204) : res.send(404,{"msg":"not found id "+idBuscar});
});


module.exports = router;
