require('../config/config');

const express = require('express');
const router = express.Router();
const usersFile = require('../users.json');
const requestJson = require('request-json');
const securityController = require('../controllers/securityController.js');
const userController = require('../controllers/userController.js');
const accountController = require('../controllers/accountController');
const movementController = require('../controllers/movementController');

//Security
router.post('/checkemail', securityController.checkemail);
router.post('/login', securityController.login);
router.post('/logout', securityController.logout);

//Users
router.get('/users', userController.getUsers);
router.get('/users/:id', userController.getUserById);
router.put('/users/:id', userController.updateUser);
router.post('/users', userController.createUser);
router.get('/users/:id/relateds', userController.relatedsUsers);

//Accounts
router.get('/users/:id/accounts', accountController.getAccounts);
router.get('/users/:id/accounts/:idaccount', accountController.getAccountById);

//Movements
router.get('/users/:id/accounts/:idaccount/movements', movementController.getMovements);
router.get('/users/:id/accounts/:idaccount/movements/:idmov', movementController.getMovementById);

module.exports = router;
