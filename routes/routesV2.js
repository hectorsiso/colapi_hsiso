require('../config/config');

const express = require('express');
const router = express.Router();
const usersFile = require('../users.json');
const requestJson = require('request-json');

router.get('/:dev', (req, res) => {
  console.log(req.params);
  res.json({ response: usersFile[req.params.dev - 1]  });
});
router.put('/:id', (req, res) => {
    var idBuscar = req.params.id;
    var updateUser = req.body;
    for(i = 0; i < usersFile.length; i++) {
      console.log(usersFile[i].id);
      if(usersFile[i].id == idBuscar) {
        updateUser = usersFile[i];
        res.send(200,{"msg" : "Usuario actualizado correctamente.", updateUser});
      }
    }
    res.send(404,{"msg" : "Usuario no encontrado.", updateUser});
});
router.delete('/:id', (req, res) => {
    var idBuscar = req.params.id;
    var d = usersFile.splice(idBuscar-1, 1);
    d.length > 0 ? res.send(204) : res.send(404,{"msg":"not found id "+idBuscar});
});
router.post('/login', (req, res) => {
  console.log(req.body);
  const user = req.body.email;
  const pass = req.body.password;
  for(u of usersFile){
    if(u.email === user){
      if(u.password === pass){
        u.logged = true;
        //guardar
        writeUserDataToFile(usersFile);
        res.send(200,{"msg":"Logged"})
      }
    }
  }
  res.send(403,{"msg": "no auth"})
});
router.post('/logout', (req, res) => {
  console.log(req.body);
  const user = req.body.email;
  const pass = req.body.password;
  for(u of usersFile){
    if(u.email === user && u.password === pass){
      if(u.hasOwnProperty("logged")){
        delete u.logged;
        //guardar
        writeUserDataToFile(usersFile);
        res.send(200,{"msg":"Log Out"})
      }
    }
  }
  res.send(403,{"msg": "no auth"})
});

function writeUserDataToFile(data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile("./users.json", jsonUserData, "utf8", (err) => {
    if(err){
      console.log(err);
    }else{
      console.log("Datods guardados");
    }
  })
}

module.exports = router;
