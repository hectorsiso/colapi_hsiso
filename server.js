const express = require('express');
const routes = require('./routes/routesV3');
const app = express();
const cors = require('cors');

const logger = require('morgan');
const mongoose = require('mongoose');
const jsonParser = express.json();

app.use(cors());
app.use(jsonParser);
app.use('/dev/v3', routes);


const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`Web server listening on: ${port}`);
});
